class Offer < ApplicationRecord
  validates :starts_at, presence: true
  validates :advertiser_name, presence: true, uniqueness: true
  validates :description, presence: true, length: { maximum: 500, too_long: "%{count} characters is the maximum allowed" }
  validates :url, presence: true, format: URI::regexp(%w[http https])

  scope :active, -> { where('starts_at <= ? and (ends_at IS NULL OR ends_at >= ? )', DateTime.current, DateTime.current) }
  scope :ordered_by_premium, -> { order(premium: :desc) }

  def enabled
    return true if ends_at.nil?

    now = DateTime.current
    starts_at <= now && ends_at >= now
  end
end
