module Admin
  class OffersController < ApplicationController
    before_action :set_offer, only: [:edit, :update, :destroy]

    def index
      @offers = Offer.all
    end

    def new
      @offer = Offer.new
    end

    def create
      @offer = Offer.new(offer_params)
      if @offer.save
        redirect_to admin_offers_path, flash: { success: 'Offer created!' }
      else
        render :new
      end
    end

    def edit
      @offer = Offer.find(params[:id])
    end

    def update
      if @offer.update(offer_params)
        redirect_to admin_offers_path, notice: 'Offer was successfully updated.'
      else
        render :edit
      end
    end

    def destroy
      @offer.destroy
      redirect_to admin_offers_path, notice: 'Offer was successfully destroyed.'
    end

    private

    def set_offer
      @offer = Offer.find(params[:id])
    end

    def offer_params
      params.require(:offer).permit(
        :advertiser_name,
        :url,
        :description,
        :starts_at,
        :ends_at,
        :premium
      )
    end
  end
end
