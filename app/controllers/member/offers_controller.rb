module Member
  class OffersController < ApplicationController
    def index
      @offers = Offer.active.ordered_by_premium
    end
  end
end
