# Lemoney Interview

## Requirements
* Ruby 2.6.4

## Install
* git clone https://futrica@bitbucket.org/futrica/lemoney.git
* cd lemoney
* bundle install
* rails db:create
* rails db:migrate
* rails db:seed
* rails s 

## Test
* bundle exec rspec
