 require 'rails_helper'

RSpec.describe "admin/offers", type: :request do
  let(:valid_attributes) do
    {
      advertiser_name: 'name',
      url: 'http://url.com',
      description: 'descr',
      starts_at: Date.current.to_s,
      ends_at: (Date.current + 1.day).to_s
    }
  end

  let(:invalid_attributes) do
    {
      advertiser_name: '',
      url: 'http://url.com',
      description: 'descr',
      starts_at: Date.current.to_s,
      ends_at: (Date.current + 1.day).to_s
    }
  end

  describe "GET /index" do
    it "renders a successful response" do
      Offer.create! valid_attributes
      get admin_offers_url
      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it "renders a successful response" do
      get new_admin_offer_url
      expect(response).to be_successful
    end
  end

  describe "GET /edit" do
    it "render a successful response" do
      offer = Offer.create! valid_attributes
      get edit_admin_offer_url(offer)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Offer" do
        expect {
          post admin_offers_url, params: { offer: valid_attributes }
        }.to change(Offer, :count).by(1)
      end

      it "redirects to index" do
        post admin_offers_url, params: { offer: valid_attributes }
        expect(response).to redirect_to(admin_offers_url)
      end
    end

    context "with invalid parameters" do
      it "does not create a new Offer" do
        expect {
          post admin_offers_url, params: { offer: invalid_attributes }
        }.to change(Offer, :count).by(0)
      end

      it "renders a successful response (i.e. to display the 'new' template)" do
        post admin_offers_url, params: { offer: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do
      let(:new_attributes) do
        {
          advertiser_name: 'new name',
          url: 'http://url.com',
          description: 'descr',
          starts_at: Date.current.to_s,
          ends_at: (Date.current + 1.day).to_s
        }
      end

      it "updates the requested offer" do
        offer = Offer.create! valid_attributes
        patch "#{admin_offers_url}/#{offer.id}", params: { offer: new_attributes }
        expect(offer.reload.advertiser_name).to eq('new name')
      end

      it "redirects to the offer" do
        offer = Offer.create! valid_attributes
        patch "#{admin_offers_url}/#{offer.id}", params: { offer: new_attributes }
        expect(response).to redirect_to(admin_offers_url)
      end
    end

    context "with invalid parameters" do
      it "renders a successful response (i.e. to display the 'edit' template)" do
        offer = Offer.create! valid_attributes
        patch admin_offer_url(offer), params: { offer: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested offer" do
      offer = Offer.create! valid_attributes
      expect {
        delete admin_offer_url(offer)
      }.to change(Offer, :count).by(-1)
    end

    it "redirects to the offers list" do
      offer = Offer.create! valid_attributes
      delete admin_offer_url(offer)
      expect(response).to redirect_to(admin_offers_url)
    end
  end
end

