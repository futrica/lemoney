require 'rails_helper'

RSpec.describe Offer, type: :model do
  describe 'validations' do
    subject do
      Offer.new(
        advertiser_name: 'teste_ad',
        starts_at: DateTime.now,
        description: 'ad description',
        url: 'http://teste.com'
      )
    end

    it { is_expected.to validate_presence_of(:starts_at) }
    it { is_expected.to validate_presence_of(:advertiser_name) }
    it { is_expected.to validate_uniqueness_of(:advertiser_name) }
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_presence_of(:url) }

    describe 'url format' do
      describe 'invalid offer' do
        it 'should be invalid' do
          invalid_subject = subject
          invalid_subject.url = 'asd'

          expect(invalid_subject).to be_invalid
        end
      end

      describe 'valid offer' do
        it 'should be valid' do
          valid_subject = subject
          valid_subject.url = 'http://asd.com'

          expect(valid_subject).to be_valid
        end
      end
    end

    describe 'description size' do
      describe 'invalid size' do
        it 'should be invalid' do
          invalid_subject = subject
          invalid_subject.description = 'a' * 501

          expect(invalid_subject).to be_invalid
        end
      end

      describe 'valid size' do
        it 'should be valid' do
          valid_subject = subject
          valid_subject.description = 'asd'

          expect(valid_subject).to be_valid
        end
      end
    end
  end
end
